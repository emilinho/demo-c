#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/moduleparam.h>
#include <linux/stat.h>

int entero = 0;
module_param(entero, int, 0);
MODULE_PARM_DESC(entero, "Número a imprimir");

char *cadena = "Hello";
module_param(cadena, charp, 0);
MODULE_PARM_DESC(cadena, "Cadena a imprimir");

int arreglo[3];
int sizeee;
module_param_array(arreglo, int, &sizeee, 0);
MODULE_PARM_DESC(arreglo, "Arreglo a imprimir");


int __init mod_init(void) {
    int i = 0;
    printk(KERN_INFO "%s mundo! entero: %d\n", cadena, entero);
    for (i = 0; i < sizeee; i++) {
        printk(KERN_INFO "%d\n", arreglo[i]);
    }
    return 0;
}


void __exit mod_cleanup(void) {
    printk(KERN_INFO "Adios mundo!\n");
    return;
}

module_init(mod_init);
module_exit(mod_cleanup);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Emilio Cabrera");
MODULE_DESCRIPTION("Modulo de prueba");