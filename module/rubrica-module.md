# Universidad Nacional Autónoma de México

# Facultad de Ciencias

## Practica 3: Módulo del kernel de Linux

* Fecha de entrega: 8 de Febrero de 2021

## Objetivo

Crear un módulo del kernel *out of tree* como el que se vio en clase.

## Descripción

El módulo tomará 3 posibles parámetros:
    - El primero, de nombre `entero` será un entero sin signo de 8 bits, este 
      parámetro tendrá prioridad sobre los otros parámetros. 
    - El segundo, de nombre `cadena` será un arreglo de caracteres de máximo 
      32 elementos, este tendrá prioridad sobre el último parametro. 
    - El tercero, de nombre `arreglo` es un `arreglo de enteros` de máximo 
      5 elementos.

**NOTA:** Cuando se pruebe el código estos parámetros no serán cero, la cadena vacía
o un arreglo de ceros.

El módulo registrará un nuevo dispositivo de caracteres en el kernel, e imprimirá 
el número asignado para ese dispositivo en el *log* del kernel.

El módulo deberá eliminar el dispositivo de caracteres del kernel antes de ser 
eliminado del kernel y solo si no hay nadie usándolo.

El módulo deberá implementar las funciones `open`, `release`, `write` y `read`
como estan definidas en la estructura `file_operations` del encabezado 
[`linux/fs.h`](https://elixir.bootlin.com/linux/v5.4/source/include/linux/fs.h#L1814)

    - La función `open` deberá informar al kernel que el módulo esta en uso y
      no puede ser eliminado usando `rmmod`
    - La función `release` deberá informar al kernel que ya no esta siendo 
      usado.
    - La función `write` deberá devolver error cuando intente ser usada.
    - La función `read` deberá copiar al *buffer* el parametro de mayor prioridad
      que haya sido proporcionado al modulo según la prioridad mencionada arriba
      y tantas veces como quepa en el *buffer*.

El módulo debe incluir la siguiente información:
    - Nombre del módulo
    - Descripción del modulo
    - Autoræs
    - La licencia
    - Descripción de todos los parámetros del módulo

Puntos extra:
    - Implementar una funcionalidad similar a la de /dev/null cuando se escriba 
      en el archivo (1 puntos extra).
    - Cuando se escriba una cadena en el archivo, la salida al leerlo será esa 
      cadena (2 puntos extra).
      ```
      $ sudo insmod ./practica3.ko cadena="Hello "
      $ sudo mknod /dev/practica3 c <numero_mayor> 0
      $ cat /dev/practica3
      Hello Hello Hello Hello Hello Hello Hello Hello Hello
      $ echo "Hola mundo" > /dev/practica3
      $ cat /dev/practica3
      Hola mundo Hola mundo Hola mundo Hola mundo Hola mundo
      ```
    - Imprimir el parámetro `arreglo` ordenado en el buffer (1 puntos extra).

## ¿Qué se califica?

**NOTA**: Si no se puede compilar y ejecutar el equipo tendrá `0` :D

* Funcionamiento del modulo - `50%`
    * Uso correcto de las funciones y encabezados del kernel
    * Uso de parametros en el módulo
    * Soporte de las funciones `open`, `release`, `read` y `write`.
    * Uso correcto de memoria (no deben haber fugas de memoria)
* Calidad del código - `20%`
    * Que el código sea consistente. Ej: No mezclar identaciones, no mezclar estilos, 
    * Separación del código y correcta organización
    * Comentarios en código
        * Cualquier operacióón no obvia deberá estar comentada
* Manejo de errores - `10%`
* Documentación del proyecto - `20%`
    * Especificar nombre del módulo
    * Autoræs
    * Incluir un `MakeFile`
    * Lista de características del módulo. ¿Qué puede hacer y qué no?
    * Lista de *bugs* conocidos

## Referencias y recursos

- [The Linux Kernel Module Programming Guide](https://tldp.org/LDP/lkmpg/2.6/html/index.html)
