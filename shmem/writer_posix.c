#include <stdio.h>     // printf, getchar, fflush
#include <string.h>    // strcpy
#include <unistd.h>    // sleep
#include <sys/stat.h>  // constantes
#include <fcntl.h>     // control de archivos
#include <sys/mman.h>  // manejo de memoria (mmap)

#define SHM_SIZE 128
#define SHM_NAME "/myshmem"

int main(int argc, char *argv[])
{
    int fd = shm_open(SHM_NAME, O_RDWR|O_CREAT|O_EXCL, 0660);
    printf("[WRITER] Nuevo segmento de memoria creado: %d\n", fd);
    
    ftruncate(fd, SHM_SIZE);
    puts("[WRITER] Segmento redimensionado");
    
    void *shmaddr = mmap(NULL, SHM_SIZE, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
    printf("[WRITER] Segmento %d mapeado a la direccion: %X\n", fd, shmaddr);
    fflush(NULL);
    getchar();

    printf("[WRITER] Escribiendo...\n");
    fflush(NULL);
    for (int offset = 0; offset < SHM_SIZE; offset += 12) {
        strncpy(shmaddr + offset, "hola mundo! ", 13);
        sleep(1);
    }

    puts("[WRITER] Desmapeando el segmento de memoria compartida");
    munmap(shmaddr, SHM_SIZE);
    puts("[WRITER] Marcando el segmento para ser eliminado");
    shm_unlink(SHM_NAME);
    close(fd);
    puts("[WRITER] Fin\n");
    fflush(NULL);

    getchar();

    return 0;
}