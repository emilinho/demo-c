#include <linux/mman.h>
#include <stdio.h>
#include <string.h> // strcpy
#include <sys/types.h> // pid_t
#include <sys/mman.h> // mmap(2)
#include <sys/wait.h> // wait(2)
#include <unistd.h> // fork(2)

int main(int argc, char *argv[])
{
    void *buf = mmap(NULL, 1024, PROT_READ|PROT_WRITE, MAP_PRIVATE | MAP_ANON, -1, 0);

    if (fork() == 0)  { // hijo
        sleep(1);
        puts("[Hijo] Imprimiendo el buffer");
        printf("%s", buf);
    } else { // padre
        puts("[Padre] Escribiendo el buffer");
        strcpy(buf, "Hola mundo!\n");
        wait(NULL); 
    }
    
    munmap(buf, 1024);
}