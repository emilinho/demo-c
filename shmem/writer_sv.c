#include <stdio.h>    // printf, getchar, fflush
#include <string.h>   // strcpy
#include <unistd.h>   // sleep
#include <sys/ipc.h>  // comunicacion entre procesos de SysV
#include <sys/shm.h>  // memoria compartida

#define SHM_SIZE 128

int main(int argc, char *argv[])
{
    int shmid;
    shmid = shmget(IPC_PRIVATE, SHM_SIZE, IPC_CREAT | IPC_EXCL | 0660);
    printf("[WRITER] Nuevo segmento de memoria creado: %d\n", shmid);
    fflush(NULL);

    void *shmaddr;
    shmaddr = shmat(shmid, NULL, SHM_R|SHM_W);
    printf("[WRITER] Segmento %d asociado a la direccion: %X\n", shmid, shmaddr);
    fflush(NULL);
    getchar();

    printf("[WRITER] Escribiendo...\n");
    fflush(NULL);
    for (int offset = 0; offset < SHM_SIZE; offset += 12) {
        strncpy(shmaddr + offset, "hola mundo! ", 13);
        sleep(1);
    }

    puts("[WRITER] Desasociando el segmento de memoria compartida");
    shmdt(shmaddr);
    puts("[WRITER] Marcando el segmento para ser eliminado");
    // shmctl(shmid, IPC_RMID, NULL);
    puts("[WRITER] fin\n");
    fflush(NULL);

    getchar();

    return 0;
}