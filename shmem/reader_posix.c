#include <fcntl.h>
#include <stdio.h>    // printf, getchar, fflush
#include <stdlib.h>   // atoi
#include <string.h>   // strcpy
#include <sys/mman.h> // mmap
#include <unistd.h>   // sleep

#define SHM_SIZE 128
#define SHM_NAME "/myshmem"

int main(int argc, char *argv[])
{

    int fd = shm_open(SHM_NAME, O_RDONLY, 0660);
    puts("[READER] Abriendo el segmento de memoria compartida");
    
    void *shmaddr = mmap(NULL, SHM_SIZE, PROT_READ, MAP_SHARED, fd, 0);
    printf("[READER] Segmento asociado a la dirección %X\n", shmaddr);
    fflush(NULL);

    int n = 0;
    printf("[READER] Leyendo...\n");
    do {
        n = printf("%s\n", shmaddr);
        fflush(NULL);
        sleep(1);
    } while (n < SHM_SIZE - 15);

    puts("[READER] Desasociando el segmento de memoria compartida");
    munmap(shmaddr, SHM_SIZE);
    close(fd);
    puts("[READER] Fin");

    return 0;
}