#include<stdio.h>
#include<signal.h>
#include<unistd.h>
#include<stdlib.h>
#include<sys/wait.h>

void handler(int signum, siginfo_t *siginfo, void *ucontext){
    printf("******** Manejador ********\n");
    if(signum == SIGCHLD){ // Murio un hijo
        printf("Murio mi hijo. Torito!!! :(\n");
        printf("Informacion del chameque:\n");
        printf("PID | UID | STATUS CODE | USR CPU TIME\n\n");
        printf("%d | %d | %d | %d \n", siginfo->si_pid, siginfo->si_uid, siginfo->si_status, siginfo->si_utime);
        waitpid(siginfo->si_pid, NULL, 0);
    }else{
        printf("Ya no sirvo, devo morir :(\n");
        printf("Información--> PID: %d, UID: %d\n", siginfo->si_pid, siginfo->si_uid);
        exit(100);
    }
}

int main(int argc, char *argv[])
{
    struct sigaction act = {0};
    pid_t un_pid;

    printf("[PADRE] Hola, mi PID es: %d\n\n", getpid());

    // Puede ser:
    // SIG_DFL
    // SIG_IGN
    // un apuntador a la funcion que manejara la señal
    act.sa_sigaction = &handler;
    // Especificar SA_SIGINFO para uso de manejador con mas info
    act.sa_flags = SA_SIGINFO;

    un_pid = fork();

    if(un_pid == 0){ /* El hijo */
        printf("\t\t[HIJO] Soy el hijo. PID: %d\n", getpid());
        if(sigaction(SIGINT, &act, NULL) < 0){
            printf("[ERROR]");
            return 1;
        }
        while(1); // Haciendo cosas
    }else{ /* Padre */
        if(sigaction(SIGCHLD, &act, NULL) < 0){
            printf("[ERROR]");
        }
        printf("[PADRE] Here we go again ...\n");
        fflush(NULL);
        sleep(3);
        kill(un_pid, SIGINT); // Mandando señal al hijo
        while(1);
    }

    return 0;
}
