#include<stdio.h>
#include<signal.h>
#include<unistd.h>
#include<stdlib.h>

void signal_callback_handler(int signum)
{
  printf("\t\n***Cache la señal %d***\n",signum);
  exit(signum);
}

int main(int argc, char *argv[])
{
      pid_t pid = getpid();

      /* Print pid, so that we can send signals from other shells */
      printf("PID: %d\n", pid);
      /* Register signal and signal handler */
      printf("Registrando SIGINT\n");
      signal(SIGINT, signal_callback_handler);
      sleep(5);
      pid = fork();

      if (pid == 0){
          signal(SIGINT, SIG_IGN);
          printf("Este es el hijo\n");
      }
      else{
          printf("Este es el padre\n");
      }
      sleep(5);
      return 0;

}
