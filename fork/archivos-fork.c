#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>

/* Define constantes globales */
#define ARCHIVO "/tmp/archivo.txt"
#define NON_BLOCK
#define SLEEP
#define BUF_LEN 255
char *MENSAJE = "aeiou";
// char *MENSAJE = "abcdefghijklmnopqrstuvwxyz";
// char *MENSAJE = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
// char *MENSAJE = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

void error(char * msg)
{
  perror(msg);
  exit(EXIT_FAILURE);
}

int main(void)
{
  /* Elimina el contenido del archivo */
  truncate(ARCHIVO, 0);
  /* Abre ARCHIVO y regresa un descriptor de archivo */
  int arch1_fd = open(ARCHIVO, O_RDWR|O_TRUNC|O_CREAT, S_IRWXU|S_IRGRP|S_IROTH);

  /* Utiliza el descriptor de archivo para obtener un apuntador a archivo */
  FILE * arch1_fp = fdopen(arch1_fd, "w+");
  /* Regresa el apuntador de posición al inicio del archivo */
  rewind(arch1_fp);

  /* Crea un proceso hijo */
  pid_t pid = fork();

  /* Error en la creación del proceso hijo */
  if (pid < 0)
    error("Error en fork(2)");

  /* Código que ejecuta el proceso padre */
  if (pid > 0)
  {
    /* Obtiene los identificadores de proceso */
    pid_t pid_padre = getpid();
    pid_t pid_hijo = pid;

    /* Inicializa el búffer de envío */
    char buffer[BUF_LEN] = "";

    /* Imprime una línea de identificación */
    printf("Soy el proceso padre\t(pid_padre: %d) (pid_hijo: %d)\n", pid_padre, pid_hijo);
    fprintf(stderr, "\tImprimiendo líneas en: %s\n", ARCHIVO);

    /* Escribe un mensaje de manera incremental en el archivo abierto */
    for(int offset=1 ; offset<=strlen(MENSAJE) ; offset++)
    {
        /* Inicializa el buffer con '\0' */
        memset(buffer,'\0',BUF_LEN);
        /* Copia una parte de MENSAJE hasta offset */
        strncpy(buffer, MENSAJE, offset);
        /* Agrega un retorno de línea al final de la cadena */
        buffer[offset] = '\n';
        /* Escribe en el pipe el bloque de caracteres buffer */
        write(arch1_fd, buffer, BUF_LEN);
    }
    /* Forza la escritura en todos los archivos abiertos */
    fflush(NULL);
    /* Cierra el descriptor de archivo */
    close(arch1_fd);
    /* Espera a que el proceso hijo termine */
    wait(NULL);
    /* Elimina el contenido del archivo */
    truncate(ARCHIVO, 0);
    /* Borra el archivo */
    unlink(ARCHIVO);
  }

  /* Código que ejecuta el proceso hijo */
  else /* pid == 0 */
  {
    /* Obtiene los identificadores de proceso */
    pid_t pid_padre = getppid();
    pid_t pid_hijo = getpid();

    /* Inicializa el búffer de recepción */
    char buffer[BUF_LEN] = "";

    /* Duplica el descriptor de archivo */
    int arch2_fd = dup(arch1_fd);
    /* Cierra el descriptor de archivo original */
    close(arch1_fd);

    /* Utiliza el descriptor de archivo para obtener un apuntador a archivo */
    FILE * arch2_fp = fdopen(arch2_fd, "r+");
    /* Regresa el apuntador de posición al inicio del archivo */
    rewind(arch2_fp);

    /* Imprime una línea de identificación */
    printf("Soy el proceso hijo\t(pid_padre: %d) (pid_hijo: %d)\n", pid_padre, pid_hijo);
    fprintf(stderr, "\tLeyendo líneas desde: %s\n", ARCHIVO);

    /* Espera a que el proceso padre termine de escribir en el archivo */
    sleep(1);

    /* Lee línea a línea el contenido del archivo abierto */
    int i = 1;
    ssize_t n = read(arch2_fd, buffer, BUF_LEN);
    while (n > 1)
    {
#ifdef SLEEP
      /* Inserta una pausa */
      sleep(2);
#endif

      /* Imprime el contenido de la línea que se guardó en el búffer */
      printf("buffer: (%d)\t%s", i, buffer);
      /* Limpia el buffer despues de usarlo */
      memset(buffer,'\0',BUF_LEN);
      i++;
      /* Lee del pipe el siguiente bloque de caracteres */
      n = read(arch2_fd, buffer, BUF_LEN);
    }

    /* Cierra el archivo */
    close(arch2_fd);
  }

  return EXIT_SUCCESS;

