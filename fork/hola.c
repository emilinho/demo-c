/* Fuente: https://www.geeksforgeeks.org/fork-system-call/
 * Desc: Programa simple que ilustra el uso de la llamada fork(2)
 */
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

int main()
{
    int i = 0;
    // make two process which run same
    // program after this instruction
    fork();
    // TODO: Agregar mas forks

    printf("Hello world!\n");
    return 0;
}

