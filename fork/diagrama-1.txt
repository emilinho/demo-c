Fuente: https://www.geeksforgeeks.org/fork-system-call/

fork ();   // Fork 1
fork ();   // Fork 2
fork ();   // Fork 3

Proceso principal: P0
Procesos creados por el fork 1: P1
Procesos creados por el fork 2: P2, P3
Procesos creados por el fork 3: P4, P5, P6, P7

             P0
         /   |   \
       P1    P4   P2
      /  \          \
    P3    P6         P5
   /
 P7
