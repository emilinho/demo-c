#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

void b();

int main(int argc, char *argv[])
{
    b();
    return 0;
}

void b(){
    printf("PID: %d\n", getpid());
    fflush(NULL);
    fork();
    b();
}
