# Universidad Nacional Autónoma de México

# Facultad de Ciencias

## Practica 1: webserver

* Fecha de entrega: 8 de diciembre 2020

## Objetivo

Aplicar los conceptos de **procesos**, **llamadas al sistema** y **señales** en el desarrollo de un servidor web en C. 

## Descripción

* Configuración como argumentos
* Tomar el esqueleto del servidor web programado en las sesiones previas y extender su funcionamiento para soportar 
multi-procesamiento.
* Implementación de los metodos `GET` (Solo para servir archivos), `POST` (Solo para subir archivos ) y `DELETE` 
(solo para eliminar archivos)
* Cuando se conecte un nuevo cliente lanzar un proceso que se encargue de atender todas sus peticiones. 
* Cuando se envie una señal de finalización (SIGINT) al proceso padre este deberá instruir a todos sus sub procesos de 
finalizar las conexiones abiertas, cerrar los archivos abiertos y terminar su ejecución.
* Generar una bitacora (log en archivo o syslog) (100%)
* Manejo de errores

## ¿Qué se califíca?

**NOTA**: Si no se puede compilar y ejecutar el equipo tendrá `0` :D

* Funcionamiento del servidor - `40%`
    * Uso correcto de llamadas al sistema y la biblioteca de C
    * Manejo adecuado de procesos
    * Uso adecuado de señales
    * Soporte de métodos `GET`, `POST` y `DELETE` (10% extra)
    * Uso correcto de memoria (no deben haber fugas de memoria)
* Calidad del código - `20%`
    * Que el código sea consistente. Ej: No mezclar identaciones, no mezclar estilos, 
    * Separación del código y correcta organización
    * Comentarios en código
        * Cualquier operación no obvia deberá estar comentada
* Manejo de errores - `10%`
* Trabajo en equipo - `20%`
    * La historia en git muestra que todo el equipo trabajo en la practica
    * La historia en git muestra roles mas o menos definidos dentro del equipo
* Documentación del proyecto - `20%`
    * Especificar nombre del proyecto
    * Autoræs
    * ¿Como compilarse y ejecutarse?
        * Incluir un `MakeFile`
            * Ejecutar el comando `make` en la carpeta de la practica debe compilar el servidor
            * Ejecutar el comando `make clean` en la carpeta de la practica debe limpiar los archivos resultantes de la 
            compilación
    * Lista de características del servidor. ¿Qué puede hacer y qué no?
    * Lista de *bugs* conocidos

Observaciones:
* Entrega en el repositorio


### Descripción detallada del funcionamiento del servidor

- De los argumentos en la linea de comandos:

    - El usuario debe poder:

        - Establecer el `root path` desde el cual se serviran todos los archivos (bandera -r)
        - Establecer la direccion IPv4 en la que el servidor escuchara por nuevas conexiones (bandera -a)
        - Establecer el puerto en el que se escuchara por nuevas conexiones (bandera -p)
        - Establecer el número máximo de procesos a lanzar para atender nuevas conexiones (bandera -t)
        - Omitir todos parametros y el programa deberá usar los siguientes valores predeterminados:

            - IP: 127.0.0.1
            - Puerto: 8080
            - Root path: directorio actual
            - Número máximo de procesos: 4
    
    - El programa debe de imprimir un error indicando al usuario que hizo mal y mostrar un mensaje recordando el uso del
    programa y los valores predeterminados cuando:

        - El usuario introduzca una ip incorrecta (formato n.n.n.n, n va 0 a 255)
        - El usuario introduzca un puerto fuera del rango (1 - 65535)
        - El usuario introduzca un `root path` no existente o al que no tiene permiso de acceder
        - El usuario introduzca un numero de procesos mayor a 80

- De las urls aceptadas:

    - No se debe de poder subir a directorios encima del `root path` establecido por el usuario
    - El servidor debe poder recibir urls con peticiones `GET` (parametros en la url),  incluso si no es capaz de 
    interpretarlas
    - Debe aceptar urls de archivos y servirlos si la ruta esta dentro del `root path`
    - Si la url es una carpeta se debe servir el archivo por defecto `index.html`
    - (Puntos Extra) El servidor puede interpretar y servir urls relativas
    - (Puntos Extra) El servidor puede interpretar y servir urls con caracteres fuera del rango de ASCII

- De la conexión con los clientes:

    - Se debe mantener la conexión con el cliente hasta que se reciba la indicación de cerrarla con un encabezado 
    `Connection: close` o se hayan recibido más de 5 peticiones del mismo cliente.
    - Cuando el servidor vaya a cerrar una conexión con un cliente debe enviar el encabezado `Connection: close` en la 
    siguiente respuesta que envíe al cliente.

- De las respuestas a los clientes:

    - Deben estar bien formadas de acuerdo al RFC7230 y los demás RFCs pertinentes
    - Todas las respuesta deben especificar la fecha y hora en el formato y zona horaria adecuadas
    - Si se sirve un archivo se debe especificar:
        - MIME Type
        - Tamaño del archivo servido

- De las peticiones GET:

    - Las peticiones mal formadas deben ser respondidas con la respuesta adecuada.
    - Las peticiones que no se puedan completar deben ser respondidas con la respuesta adecuada.
    - (Puntos extra) Implementar peticiones HEAD
    - (Puntos extra) Responder con la respuesta adecuada si el servidor no es capaz de preparar cafe.

- De las peticiones POST:

    - Las peticiones POST solo deben crear archivos, no es necesario implementar funcionalidad adicional
    - Las peticiones POST solo deben ser aceptadas en la url `/uploads`
    - Se debe guardar el archivo de acuerdo al nombre indicado en la petición
    - El servidor debe honrar el encabezado `Expect: 100-continue`
    - Las subidas exitosas deben ser señaladas al cliente usando la respuesta adecuada.
    - Las peticiones mal formadas deben ser respondidas con la respuesta adecuada.
    - Las peticiones POST serán realizadas usando curl de la siguiente manera:

```
    curl -F=@ruta/al/archivo.ext 127.0.0.1:8080/upload
```

- (Puntos Extra) De las peticiones DELETE:

    - Las peticiones DELETE deben eliminar el archivo solicitado en la url
    - Se debe responder en caso de exito de forma adecuada
    - Se debe responder de forma adecuada en caso de error

- De los procesos lanzados:

    - Una vez lanzado un nuevo procesos hijo este debe cerrar el socket del servidor
    - Una vez lanzado un nuevo proceso hijo el padre debe cerrar el socket obtenido de `accept`
    - Cuando se envie una señal al proceso padre de finalización los procesos hijos deben terminar de forma limpia, 
    cerrando archivos, liberando memoria y cerrando conexiones

- De los errores y su registro:

    - Todas las funciones que señalicen un error deben ser manejadas al menos imprimiendo el error en `stderr` y en caso
    de ser necesario terminando el proceso.
    - Todas la conexiones nuevas, peticiones y respuestas deben imprimirse en `stderr` (solo `request-line` y `status line`)
    - (Puntos Extras) En lugar de imprimir en `stderr` imprimir en el syslog del sistema

## Forma de entrega


La entrega será en el repositorio de [tareas-so](https://gitlab.com/SistemasOperativos-Ciencias-UNAM/2021-1/tareas-so)
siguiendo el procedimiento y estructura de carpetas descrito en el mismo repositorio.

Un solo una integrante del equipo debe hacer el merge request de la práctica, el resto de los integrantes deberán hacer 
un merge request que contenga solo una liga simbólica a la carpeta del integrante que entrega la práctica.

Para crear el enlace simbólico a la carpeta de otro compañero pueden seguir este procedimiento:

0. Asegurarse que su repositorio local este actualizado con el repositorio en el que esta trabajando su equipo.
1. Asegurarse que la carpeta de la práctica exista en algun commit anterior
2. Crear la liga simbolica
```
$ cd content/tarea/MiNombre
$ ln -s ../../MiCamarada/practicas/practica-1/ .
```
3. Crear un commit que añada la liga simbolica al repositorio
```
$ git add practica-1 # NO incluir un slash (/) al final
$ git commit -m "mi mensaje de commit"
```
4. Hacer push y crear el merge request

### Referencias y recursos

* [HTTP Made Really Easy](https://www.jmarshall.com/easy/http/)
* [Beej's Guide to Network Programming](https://beej.us/guide/bgnet/)
* [Implementing a TCP server in C](https://ops.tips/blog/a-tcp-server-in-c/)
