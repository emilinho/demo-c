# Universidad Nacional Autónoma de México

# Facultad de Ciencias

## Practica 2: Webserver orientado a hilos

* Fecha de entrega: 28 de Enero 2021

## Objetivo

Aplicar los conceptos de **hilos** en el desarrollo de un servidor web en C. 

## Descripción

* Se tomará como base el servidor desarrollado en la práctica 1 para adaptarlo a hilos.
* El hilo principal se encargará de recibir las conexiones entrantes y lanzar hilos para atenderlas.
* Los hilos lanzados por el hilo principal deben devolver informacion al hilo principal al final de su ejecución sobre el id del hilo (usando pthread_self), el cliente al que atendió y el número de peticiones que despacho.
* Los hilos deben cerrar el socket que usaron para atender peticiones antes de terminar.
* No se deben de usar llamadas al sistema o de la biblioteca de C que no sean seguras entre hilos (thread-safe).

## ¿Qué se califíca?

La forma de evaluación es la misma que en la práctica 1 pero esta ve se evaluará el uso correcto de hilos y el uso de funciones seguras entre hilos.

**NOTA**: Si no se puede compilar y ejecutar el equipo tendrá `0` :D


### Descripción detallada del funcionamiento del servidor (diferencias con la practica 1)

- De los argumentos en la linea de comandos:

    - El argumento -t debe establecer el numero máximo de hilos a lanzar

- De la conexión con los clientes:

    - Se debe mantener la conexión con el cliente hasta que se reciba la indicación de cerrarla con un encabezado 
    `Connection: close` o se hayan recibido más de 5 peticiones del mismo cliente.
    - Cuando el servidor vaya a cerrar una conexión con un cliente debe enviar el encabezado `Connection: close` en la 
    siguiente respuesta que envíe al cliente.

- Del hilo principal:

    - Debe manejar la señal SIGINT de la siguiente manera:

        - Debe dejar de recibir conexiones entrantes (cerrar socket)
        - Debe esperar a que finalicen todos los hilos que lanzó
        - Debe imprimir la información que e devuelvan los hilos al finalizar

- De los hilos lanzados:

    - Los hilos NO deben cerrar el socket del hilo principal
    - El hilo principal tampoco debe cerrar los sockets de los hilos lanzados
    - Los hilos deben cerrar el socket con el que atienden peticiones al finalizar
    - No deben manejar ninguna señal
    - Los hilos deben devolver la siguiente información al hilo principal al terminar:

        - Thread ID
        - Hora en que finalizaron
        - El cliente y puerto en que atendio
        - Número de peticiones que despacho


### Referencias y recursos

* [POSIX Threads Programming](https://computing.llnl.gov/tutorials/pthreads/)