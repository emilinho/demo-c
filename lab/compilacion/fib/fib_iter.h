#ifndef FIB_ITER_H
#define FIB_ITER_H // include guard

unsigned int fib_iter(unsigned int n);
void fib(unsigned int *buff, unsigned int n);

#endif // FIB_ITER_H