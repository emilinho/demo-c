#include "fib.h"
#include <stdio.h>

void fib(unsigned int *buff, unsigned int n) {
    printf("Version recursiva\n");
    for (unsigned int i = 0; i < n; i++) {
        *(buff + i) = fib_rec(i);
    }
}

unsigned int fib_rec(unsigned int n)
{
    if (n <= 1) {
        return n;
    }
    return fib_rec(n - 1) + fib_rec(n - 2);
}