// @author: Emilio Cabrera <emilio1625@ciencias.unam.mx>

// Arreglos y apuntadores
#include <stdio.h>
#include <stdint.h>

int main(int argc, char const **argv)
{
    // // | 1 | 2 | 3 | 4 | 5 |
    // int arr1[5]; // tamaño
    // int arr2[] = {4, 5, 6, 7}; // elementos
    // int arr3[][2] = {{1, 4}, {2, 3}, {1,3}};
    // // | 'H' | 'o' | 'l' | 'a' | ... | '\0' |


    // arr1[2];

    // arr1 + 2;

    // // char str[] = "Hola";
    // // char str[] = {'H', 'o', 'l', 'a', '\0'};


    // // size_t count = 5;
    // // for (size_t i = 0; i < count; i++)
    // // {
    // //     printf("%d ", arr1[0]);
    // //     // printf("%d ", arr2[i]);
    // // }

    // float pi = 3.141592;
    // float *p = &pi;

    // int *q = (int *)p;


    // printf("%f %d\n", *p, *q);

    // char ch = 'A';
    // char *ach = &ch;

    // void *y;
    // int * t = (int *)y;

    // t = (int *) malloc(sizeof(int) * 15);

    // int8_t *aint = (int8_t *)ach;

    // printf("%d %d\n", ach, aint);

    // printf("%c %d\n", *ach, *aint);



    int arr[] = {1, 2, 3, 4};
    int *p = &arr[2];

    // ! cuidado
    printf("%u \n", arr);
    printf("%d \n", arr[0]);
    printf("%u \n", p);
    printf("%d \n", *p);
    printf("%d \n", *(p + 1));

    return 0;
}

