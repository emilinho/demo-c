// @author: Emilio Cabrera <emilio1625@ciencias.unam.mx>

#include <stdlib.h>
#include <stdio.h>

// struct book
// {
//     unsigned int id;
//     char title[12];
//     char author[12];
//     float price;
// };

typedef struct {
  int id;
  char title[12];
  char author[12];
  float price;
} Book;

int main()
{
    // setup seed
    srand(32);

    // Estas son las formas de declarar arreglos de estructuras en C
    Book library[10];
    // struct book library[10];

    for (int i = 0; i < 10; i++)
    {
        library[i].id = i;
        library[i].price = 100.0f;
        for (int j = 0; j < 10; j++)
        {
            library[i].title[j] = 97 + rand() % 26; // en ascii las letras minusculas comienzan en el 97
            library[i].author[j] = 97 + rand() % 26; // ? que hace rand() % 26 en esta sentencia?
        }
        library[i].title[10] = '\0';
        library[i].author[10] = '\0';
    }

    print_books(library, 10);

    return 0;
}

// declaracion alternativa de esta funcion, en esta version no declaramos a
// la estructura usando typedef
// void print_book(struct book my_book) {
void print_book(Book my_book) {
    printf("%d, %s, %s, %f\n", my_book.id, my_book.title, my_book.author, my_book.price);
}

// Hay dos formas de escribir funciones que reciban arreglos de structuras
// una forma es usando notacion de arreglos como la siguiente función
void print_books(Book my_books[], unsigned int n) {
    printf("id,\ttitle,\tauthor,\tprice\n");
    for (unsigned int i = 0; i < n; i++)
    {
        print_book(my_books[i]); // En esta version indexamos como en un arreglo
    }
}

// En esta funcion usamos aritmetica de apuntadores en lugar de arreglos
// void print_books(Book *my_books, unsigned int n) {
//     printf("id,\ttitle,\tauthor,\tprice\n");
//     for (unsigned int i = 0; i < n; i++)
//     {
//         print_book(*(my_books + i)); // Nota como usamos (base + desplazamiento) para acceder a un elemento
//     }
// }
