// @author: Emilio Cabrera <emilio1625@ciencias.unam.mx>

#include <stdio.h>
#include <stdlib.h>

// enum nos permite hacer tags y banderas en nuestro codigo más legibles
enum book_type {
    Novel, // ? que valor almacena Novel?
    Manual, // ? y Manual?
    Cookbook,
    Dictionary,
};

// typedef enum
// {
//     Novel,
//     Manual,
//     Cookbook,
//     Dictionary,
// } BookType;

struct book
{
    unsigned int id;
    char title[12];
    char author[12];
    enum book_type type;
    // BookType type;
    float price;
};

int main()
{
    // setup seed
    srand(33);

    // Book library[10];
    struct book library[10];

    for (int i = 0; i < 10; i++)
    {
        library[i].id = i;
        library[i].price = 100.0f;
        library[i].type = rand() % 4;
        for (int j = 0; j < 10; j++)
        {
            library[i].title[j] = 97 + rand() % 26;
            library[i].author[j] = 97 + rand() % 26;
        }
        library[i].title[10] = '\0';
        library[i].author[10] = '\0';
    }

    printf("id,\ttitle,\tauthor,\ttype,\tprice\n");
    for (size_t i = 0; i < 10; i++)
    {
        printf("%d, %s, %s, %d, %f\n", library[i].id, 
        library[i].title, library[i].author, library[i].type, library[i].price);
    }

    return 0;
}
